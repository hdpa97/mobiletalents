angular
  .module("career.controllers", [])
  .controller('ApplicantSelectionCtrl', function (
    $ionicHistory,
    $ionicLoading,
    $rootScope,
    $scope,
    $ionicModal,
    $ionicPopup,
    $state,
    $stateParams,
    $filter,
    $sce,
    AuthenticationService,
    Main) {

    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.profile = {};
    $scope.dataApp = {};
    $scope.dataUpdate = {};
    $scope.findByNpk = {
      search: ""
    };
    $scope.npkItems = [];
    $scope.showDelegateName = '';
    $scope.modalIntvGreeting = {blockIntvGreetingPopup: false};

    var profileData = Main.getSession("profile");
    $scope.isCareerAdmin = profileData.isCareerAdmin;

    $scope.today = new Date();

    $ionicModal
      .fromTemplateUrl("modal.html", {
        id: "1",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modal1 = modal;
      });


    $scope.openModal = function (index) {
      $scope.modal1.show();
    };

    $scope.closeModal = function (index) {
      $scope.modal1.hide();
    };

    $ionicModal
      .fromTemplateUrl("modalInterviewGuide.html", {
        id: "0",
        scope: $scope
      })
      .then(function (modal) {
        $scope.modalInterviewGuide = modal;
      });

    $scope.openModalInterviewGuide = function () {
      $scope.modalInterviewGuide.show();
    };

    $scope.closeModalInterviewGuide = function () {
      if($scope.modalIntvGreeting.blockIntvGreetingPopup){
        Main.setSession('hideIntvGuidePopup', new Date());
      }
      $scope.modalInterviewGuide.hide();
    };

    $scope.searchByNpk = function (page) {
      var size = 15;
      $scope.findStatus = false;
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() +
        "/api/employee/find?page=" +
        page +
        "&size=" +
        size +
        "&npk=" +
        $scope.findByNpk.search + 
        "&q=" + 
        $scope.findByNpk.search;
      // // (urlApi);
      Main.requestApi(
        accessToken,
        urlApi,
        successRequestNpk,
        $scope.errorRequest
      );

    };

    $scope.openBrowser = function () {
      window.open(
        "https://career.acc.co.id/uploads/guide/pertanyaan_interview.pdf",
        "_system",
        "location=yes"
      );
    }

    var successRequestNpk = function (res) {

      var infiniteLimit = 15
      $scope.isLoadMoreShow = false;
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $ionicLoading.hide();
      $scope.npkItems = [];
      $scope.isLast = res.last;
      $scope.isFirst = res.first;
      $scope.InterviewGreeting = '';

      for (var i = 0; i < res.content.length; i++) {
        var obj = res.content[i];
        $scope.npkItems.push(obj);


        if ($scope.npkItems.length >= infiniteLimit || $scope.npkItems.length <= res.numberOfElements && $scope.npkItems.length != 1) {

          $scope.isLoadMoreShow = true;
          $scope.noDataShow = false;
        } else if ($scope.npkItems.length == 0 || $scope.npkItems.length == 1) {


          $scope.isLoadMoreShow = false;
        }


        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');

      }
    }



    $scope.getValue = function (name, npk, employeeId) {      
      $scope.dataUpdate.delegateEmployeeId = employeeId;
      $scope.showDelegateName = name;

      $scope.modal1.hide();

      $scope.findByNpk.search = '';
    };


    var indexPaging = 0;


    $scope.loadMoreNpk = function () {
      indexPaging++;
      $scope.npkItems = [];
      $scope.searchByNpk(indexPaging);
      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevNpk = function () {
      indexPaging--;
      $scope.npkItems = [];
      $scope.searchByNpk(indexPaging);
    };



    function getAppSelection() {
      var id = $stateParams.applicantId;
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/interviewSchedule/' + id;
      Main.requestApi(accessToken, urlApi, sucessData, $scope.errorRequest);


    }

    var sucessData = function (res) {
      $scope.dataApp = res;

      $rootScope.dataApplicantDetail = res;

      // data Update default
      $scope.dataUpdate.id = res.id;
      $scope.dataUpdate.delegateEmployeeId = res.delegateEmployeeId;
      if(res.delegateEmployee == null){
        $scope.showDelegateName = '';
      }else{
        $scope.showDelegateName = res.delegateEmployee.name;
        
      }

      $ionicLoading.hide();
      $scope.$broadcast("scroll.refreshComplete");
      $scope.$broadcast("scroll.infiniteScrollComplete");
    };

    $scope.refresh = function(){
      getAppSelection();
    }

    $scope.saveData = function () {
      var accessToken = Main.getSession("token").access_token;
      var data = JSON.stringify($scope.dataUpdate);

      var urlApi = Main.getUrlApi() + '/api/interviewSchedule/delegate';
      Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);
    }


    function successSave(res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack('app.interview-schedule-admin');
    }




    function initModule() {
      $scope.profile = Main.getSession("profile");
      getAppSelection();
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });

    function afterEnterModule(){
      var hideIntvGuidePopup = Main.getSession('hideIntvGuidePopup');

      if((hideIntvGuidePopup === undefined || hideIntvGuidePopup === null ||
        hideIntvGuidePopup === '' || $filter('date')(hideIntvGuidePopup, "yyyy-MM-dd") !== $filter('date')(new Date(), "yyyy-MM-dd"))
        && ($scope.dataApp.result === '' || $scope.dataApp.result === 'In Progress' || $scope.dataApp.result === null) 
        && (($filter('date')($scope.dataApp.schedule, "yyyy-MM-dd")) === $filter('date')($scope.today, "yyyy-MM-dd"))){
        $scope.getInterviewGuideGreeting();
        $scope.openModalInterviewGuide();
      }
    }

    $scope.$on('$ionicView.afterEnter', function(){
      afterEnterModule();
    });

    $scope.getInterviewGuideGreeting = function () {
      var accessToken = Main.getSession("token").access_token;

      var urlApi = Main.getUrlApi() + '/api/talentsParameter?key=INTERVIEW_GREETING';
      Main.requestApi(accessToken, urlApi, successGreeting, $scope.errorRequest);
    }

    var successGreeting = function (res) {
      if (res != null) {
        $scope.InterviewGreeting = res;
        $ionicLoading.hide();
      }
    }

    $scope.trustAsHtml = function(string) {
      return $sce.trustAsHtml(string);
    };
  })



  .controller('ProfileCandidateCtrl', 
    function (
      $stateParams, 
      $ionicLoading, 
      $rootScope, 
      $scope, 
      $state, 
      Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }



    $scope.requests = [];
    $scope.chooseTab = {};
    var profile = Main.getSession("profile");
    var id = $stateParams.id;
    $scope.isHr = profile.isHr;


    $scope.dataProfile = {};
    $scope.dataEducation = {};
    $scope.dataWork = {};
    $scope.dataOrganization = {};
    $scope.module = {};
    $scope.dataApplicantDetail = {};


    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;
      if (tab === 'profile') {
        // localStorage.setItem("valueProfile", "profile");
        getProfile(id); // harcode id 
      } else if (tab === 'education') {
        // localStorage.setItem("valueProfile", "education");
        getEducation(id);
      } else if (tab === 'organization') {
        // localStorage.setItem("valueProfile", "organization");
        getOrganization(id);
      } else if (tab === 'working') {
        $scope.attendanceRequests = [];
        // localStorage.setItem("valueProfile", "working");
        getWorkExperience(id);
      } else if (tab === 'document') {
        $scope.attendanceRequests = [];
        // localStorage.setItem("valueProfile", "document");
        // getWorkExperience(2);
      }

    }


    if ($rootScope.dataApplicantDetail != null) {
      $scope.dataApplicantDetail = $rootScope.dataApplicantDetail.applicantSelection.jobRequisition;
    }


    $scope.refresh = function () {
      $scope.chooseTab($scope.module.type);
    };

    function getProfile(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id;
      Main.requestApi(accessToken, urlApi, successProfile, $scope.errorRequest);
    }

    function getEducation(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/education';
      Main.requestApi(accessToken, urlApi, successEducation, $scope.errorRequest);
    }


    function getWorkExperience(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/workexperience';
      Main.requestApi(accessToken, urlApi, successWorkexperience, $scope.errorRequest);
    }


    function getOrganization(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicant/' + id + '/organization';
      Main.requestApi(accessToken, urlApi, successOrganization, $scope.errorRequest);
    }




    var successProfile = function (res) {
      if (res != null) {
        $scope.dataProfile = res;
        $ionicLoading.hide();
      }

    };


    var successEducation = function (res) {
      if (res != null) {
        $scope.dataEducation = res;
        $ionicLoading.hide();
      }
    };


    var successWorkexperience = function (res) {
      if (res != null) {
        $scope.dataWork = res;
        $ionicLoading.hide();
      }

    };


    var successOrganization = function (res) {
      if (res != null) {
        $scope.dataOrganization = res;
        $ionicLoading.hide();
      }
    };

    function initModule() {
      getProfile(id);
      getOrganization(id);
      getWorkExperience(id);
      getEducation(id);

      $scope.chooseTab('profile');
    }


    initModule();

  })


  .controller('InterviewScheduleCtrl', function (
    ionicDatePicker,
    $ionicLoading,
    $ionicModal,
    $scope,
    $state,
    $filter,
    Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    localStorage.setItem("value", "today");

    $scope.chooseTab = {};
    var profile = Main.getSession("profile");
    var size = Main.getDataDisplaySize();
    $scope.isHr = profile.isHr;

    $scope.module = {};
    $scope.listInterviewScheduleToday = [];
    $scope.listInterviewScheduleTomorrow = [];
    $scope.listInterviewScheduleAll = [];
    // $scope.scheduleRange = {
    //   startDate: $filter('date')(new Date(), "yyyy-MM-dd"),
    //   endDate: $filter('date')(new Date(), "yyyy-MM-dd"),
    // };

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;
      if (tab === 'today') {
        $scope.listInterviewScheduleToday = [];
        localStorage.setItem("value", "today");
        getInterviewScheduleToday();
      } else if (tab === 'tomorrow') {
        $scope.listInterviewScheduleTomorrow = [];
        localStorage.setItem("value", "tomorrow");
        getInterviewScheduleTomorrow();
      } else if (tab === 'all') {
        $scope.listInterviewScheduleAll = [];
        localStorage.setItem("value", "all");
        getThisMonthSchedule();
        // $scope.openModalAllSchedule();
        // getInterviewScheduleAll();
      }


    };

    var startDatePicker = {
      callback: function (val) { //Mandatory
        $scope.scheduleRange.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openStartDatePicker = function () {
      ionicDatePicker.openDatePicker(startDatePicker);
    };

    var endDatePicker = {
      callback: function (val) { //Mandatory
        $scope.scheduleRange.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openEndDatePicker = function () {
      ionicDatePicker.openDatePicker(endDatePicker);
    };

    $scope.gotoDetail = function (applicantId) {
      $state.go("app.applicant-selection", {
        applicantId: applicantId
      });
    };

    $scope.refresh = function () {
      $scope.chooseTab($scope.module.type);
    };


    var successInterviewScheduleToday = function (res) {
      if (res != null) {
        $scope.listInterviewScheduleToday = res;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    var successInterviewScheduleTomorrow = function (res) {
      if (res != null) {
        $scope.listInterviewScheduleTomorrow = res;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    var successInterviewScheduleAll = function (res) {
      if (res != null) {
        $scope.listInterviewScheduleAll = [];
        $scope.listInterviewScheduleAll = res.content;

        var totalR = res.totalRecord; // 42
        var countPages = totalR / size;
        var latestPage = Math.ceil(countPages);

        // calculate last 
        var fixLatestPage = latestPage - 1;

        if (res.page == fixLatestPage) {
          $scope.latest = 1;
        }

        if (res.content != null) {
          if ($scope.listInterviewScheduleAll.length < size)
            $scope.isLoadMoreShow = false;
          else
            $scope.isLoadMoreShow = true;
        } else {
          $scope.isLoadMoreShow = false;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    function getInterviewScheduleToday() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/interviewScheduleToday';

      Main.requestApi(accessToken, urlApi, successInterviewScheduleToday, $scope.errorRequest);
    }

    function getInterviewScheduleTomorrow() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/interviewScheduleTommorow';

      Main.requestApi(accessToken, urlApi, successInterviewScheduleTomorrow, $scope.errorRequest);
    }

    function getInterviewScheduleAll(scheduleRange) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var startDate = $filter('date')(scheduleRange.startDate, "yyyy-MM-dd");
      var endDate = $filter('date')(scheduleRange.endDate, "yyyy-MM-dd");
      // Pass, Failed, Divert, Skip
      var status = scheduleRange.interviewStatus;
      var page = 0;
      var size = 10;

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/interviewScheduleRange?startDate=' + startDate +
        '&endDate=' + endDate;

      if (status !== '' && status !== undefined) {
        urlApi = urlApi + '&status=' + status
      }
      urlApi = urlApi + '&page=' + page + '&size=' + size;
      Main.requestApi(accessToken, urlApi, successInterviewScheduleAll, $scope.errorRequest);
    }

    function getThisMonthSchedule() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var date = new Date();
      var vFirstDate = new Date(date.getFullYear(), date.getMonth(), 1);
      var vLastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

      var startDate = $filter('date')(vFirstDate, "yyyy-MM-dd");
      var endDate = $filter('date')(vLastDate, "yyyy-MM-dd");

      var page = 0;
      var size = 10;

      $scope.scheduleRange = {
        startDate: startDate,
        endDate: endDate,
      };
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/interviewScheduleRange?startDate=' + startDate +
        '&endDate=' + endDate;

      urlApi = urlApi + '&page=' + page + '&size=' + size;

      Main.requestApi(accessToken, urlApi, successInterviewScheduleAll, $scope.errorRequest);
    }


    $ionicModal.fromTemplateUrl('modalAllSchedule.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modalAllSchedule = modal;
    });

    $scope.openModalAllSchedule = function () {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      $scope.modalAllSchedule.show();
    };

    $scope.closeModalAllSchedule = function () {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      $scope.modalAllSchedule.hide();

      // $scope.scheduleRange.startDate = '';
      // $scope.scheduleRange.endDate = '';
      // $scope.scheduleRange.interviewStatus = '';
    };

    $scope.searchScheduleRange = function () {
      getInterviewScheduleAll($scope.scheduleRange);
      $scope.closeModalAllSchedule();
    };


    function initMethod() {
      $scope.chooseTab(localStorage.getItem("value"));
    }
  })






  .controller('PrescreeningResultCtrl', 
    function (
      $stateParams, 
      $ionicLoading, 
      $scope, 
      $state, 
      Main
      ) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.requests = [];
    $scope.prescreeningList = [];

    var data = {
      "id_user": $stateParams.idUser,
      "id_job": $stateParams.idJob
    };


    function listPresreening() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApiWebCareer() + '/api-prescreening-result';


      Main.postRequestPresreeningApi(urlApi, data, successPrescreening, $scope.errorRequest);
    }


    var successPrescreening = function (res) {
      if (res != null) {
        $scope.prescreeningList = res.result;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    }


    function initMethod() {
      listPresreening();
    }


    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });



  })



  .controller('InterviewResultCtrl', function (
    $stateParams,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    Main
    ) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }
    $scope.interviewResult = {};
    $scope.general.userPhoto = $rootScope.user.photo;
    var id = $stateParams.id;

    function listInterviewResult() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicantSelection/' + id;
      Main.requestApi(accessToken, urlApi, successInterview, $scope.errorRequest);
    }


    var successInterview = function (res) {
      if (res != null) {
        $scope.interviewResult = res;
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    function initMethod() {
      listInterviewResult();
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

  })


  .controller('InterviewResultDetailCtrl', function (
    $stateParams, 
    $ionicLoading, 
    $ionicScrollDelegate,
    $scope, 
    $state, 
    Main
    ) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.interviewSection = [];
    $scope.currentSection = 1;
    $scope.maxSection = 0;
    $scope.summary = {};

    // used to differ, if the template used to submit interview, or just view interview result.
    $scope.sectionReadOnly = true;

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

    $scope.interviewPrevious = function () {
      // get the previous interview section
      $scope.currentSection = $scope.currentSection - 1;
      getInterviewSection($scope.currentSection);
    };

    $scope.interviewNext = function () {
      // get the next interview section
      $scope.currentSection = $scope.currentSection + 1;
      if ($scope.currentSection <= $scope.maxSection) {
        getInterviewSection($scope.currentSection);
      } else {
        getInterviewSummary();
      }
    };

    var successInterviewResultDetail = function (res) {
      if (res != null) {
        $scope.interviewSection = res;

        if (res.length > 0) {
          if (res[0] !== undefined) {
            $scope.maxSection = res[0].maximumSection;
          }
        }

        $ionicScrollDelegate.scrollTop();
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    var successInterviewSummary = function (res) {
      if (res != null) {
        $scope.summary = {
          finalResult: res.result,
          remark: res.remarks,
        };
        $ionicScrollDelegate.scrollTop();
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    function initMethod() {
      getInterviewSection();
    }

    function getInterviewSection(idx) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var interviewId = $stateParams.id;
      var section = 1;

      if (idx !== null && idx > 0) {
        section = idx;
      }
      var urlApi = Main.getUrlApi() + '/api/interviewSchedule/' + interviewId + '/getform/' + section;

      Main.requestApi(accessToken, urlApi, successInterviewResultDetail, $scope.errorRequest);
    }


    function getInterviewSummary() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var interviewId = $stateParams.id;
      var urlApi = Main.getUrlApi() + '/api/interviewSchedule/' + interviewId;

      Main.requestApi(accessToken, urlApi, successInterviewSummary, $scope.errorRequest);
    }
  })




  .controller('FormInterviewCtrl', function (
    $stateParams,
    $ionicLoading,
    $rootScope,
    $scope,
    $state,
    $ionicScrollDelegate,
    ionicSuperPopup,
    Main
  ) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.interviewSection = [];
    $scope.general.userPhoto = $rootScope.user.photo;
    $scope.currentSection = 1;
    $scope.maxSection = 0;
    $scope.summary = {};

    // used to differ, if the template used to submit interview, or just view interview result.
    $scope.sectionReadOnly = false;

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

    function initMethod() {
      getInterviewSection();
    }

    var messageValidation = "";

    var successInterviewSection = function (res) {
      if (res != null) {
        $scope.interviewSection = res;

        if (res.length > 0) {
          if (res[0] !== undefined) {
            $scope.maxSection = res[0].maximumSection;
          }
        }

        $ionicScrollDelegate.scrollTop();
        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
        
      }
    };

    $scope.interviewNext = function () {
      // do save the current section answer
      // saveInterviewSection();
      if(validateInterviewSection()){
        // get the next interview section
        $scope.currentSection = $scope.currentSection + 1;
        if ($scope.currentSection <= $scope.maxSection) {
          getInterviewSection($scope.currentSection);
        }
      }
    };

    $scope.interviewPrevious = function () {
      // do save the current section answer
      if ($scope.currentSection <= $scope.maxSection) {
        // saveInterviewSection();
        if(validateInterviewSection()){
          // get the previous interview section
          $scope.currentSection = $scope.currentSection - 1;
          getInterviewSection($scope.currentSection);
        }
      }
      else {
        // from summary section no need validation
        $scope.currentSection = $scope.currentSection - 1;
        getInterviewSection($scope.currentSection);
      }

      
    };

    

    $scope.interviewFinish = function () {
      if (verificationForm($scope.summary)) {
        ionicSuperPopup.show({
            title: "Are you sure?",
            text: "Are you sure the data submitted is correct ?",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Yes",
            closeOnConfirm: true
          },
          function (isConfirm) {
            if (isConfirm) {
              saveInterviewSummary();
              // disable button for handle repeat submit
              // $scope.buttonDisable = 1;
            }
          });
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    function getInterviewSection(idx) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var interviewId = $stateParams.id;
      var section = 1;

      if (idx !== null && idx > 0) {
        section = idx;
      }
      var urlApi = Main.getUrlApi() + '/api/interviewSchedule/' + interviewId + '/getform/' + section;

      Main.requestApi(accessToken, urlApi, successInterviewSection, $scope.errorRequest);
    }

    function validateInterviewSection(){
      var validationPassed = true;

      var tmpArrData = [];
      for (var i = 0; i < $scope.interviewSection.length; i++) {
        // loop validation

        if($scope.interviewSection[i].type === 'Multiple Choice'){
          if($scope.interviewSection[i].answerRating === null ||
            $scope.interviewSection[i].answerRating === undefined ||
            $scope.interviewSection[i].answerRating === ''){
              validationPassed = false;
          }          
        }        

        if(validationPassed && 
            ($scope.interviewSection[i].type === 'Free Text' || 
              ($scope.interviewSection[i].type === 'Multiple Choice' && $scope.interviewSection[i].needRemarks))
            ){
          if($scope.interviewSection[i].answerRemark === null ||
            $scope.interviewSection[i].answerRemark === undefined ||
            $scope.interviewSection[i].answerRemark === '') {
              validationPassed = false;
          }
        }

        // push temporary array
        tmpArrData.push({
          interviewTemplateDetailId: $scope.interviewSection[i].interviewTemplateDetailId,
          interviewResultId: $scope.interviewSection[i].interviewResultId,
          answerRemark: ($scope.interviewSection[i].answerRemark !== null && $scope.interviewSection[i].answerRemark !== undefined) ? $scope.interviewSection[i].answerRemark : null,
          answerRatingId: ($scope.interviewSection[i].answerRating !== null && $scope.interviewSection[i].answerRating !== undefined) ? $scope.interviewSection[i].answerRating.id : null,
        });
      }

      if(!validationPassed){
        $scope.warningAlert("Rating and Remark can't be empty");
      }
      else {
        saveInterviewSection(tmpArrData);
      }

      return validationPassed;

    }

    function saveInterviewSection(tmpArrData) {
      var accessToken = Main.getSession("token").access_token;

      var scheduleId = $stateParams.id;
      
      var data = JSON.stringify(tmpArrData);
      // var data = JSON.stringify($scope.interviewSection);            
      var urlApi = Main.getUrlApi() + '/api/interviewresult/' + scheduleId;
      Main.postRequestApi(accessToken, urlApi, data, successSave, $scope.errorRequest);

    }

    function saveInterviewSummary() {
      var accessToken = Main.getSession("token").access_token;

      var tmpArrData = {
        interviewScheduleId: $stateParams.id,
        finalResult: ($scope.summary.finalResult !== null) ? $scope.summary.finalResult : null,
        remark: ($scope.summary.remark !== null) ? $scope.summary.remark : null,
      };

      var data = JSON.stringify(tmpArrData);
      var urlApi = Main.getUrlApi() + '/api/interviewresult/';
      Main.postRequestApi(accessToken, urlApi, data, successSaveSummary, $scope.errorRequest);

    }

    function successSave() {}

    function successSaveSummary(res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $scope.goBack('app.interview-schedule');
    }

    function verificationForm(summary) {
      if (summary.finalResult == undefined || summary.finalResult == '') {
        messageValidation = "Summary can't be empty";
        return false;
      } else if (summary.remark == undefined || summary.remark == '') {
        messageValidation = "Remark can't be empty";
        return false;
      }
      return true;
    }


   


    // function prepareInterviewSection(idx) {
    //   lsInterviewSection = Main.getSession('tempInterview');

    //   if (lsInterviewSection === undefined || lsInterviewSection[(idx - 1)] === undefined) {
    //     // kalau masih kosong ambil dari API
    //     getInterviewSection(idx)
    //   } else {
    //     $scope.interviewSection = lsInterviewSection[(idx - 1)];
    //   }
    // }

  })

  .controller('InterviewMainCtrl', 
    function (
      $state, 
      Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    // updated by JUW, will not use this function anymore
    // if not career admin, then redirect to default interview menu
    // if(Main.getSession("profile").isCareerAdmin === false){
    //   $state.go("app.interview-schedule");
    // }
  })

  .controller('InterviewScheduleAdminCtrl', function (
    ionicDatePicker,
    $ionicLoading,
    $ionicModal,
    $scope,
    $state,
    $filter,
    Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    var profile = Main.getSession("profile");
    var size = Main.getDataDisplaySize();
    var today = new Date();
    $scope.isHr = profile.isHr;

    $scope.module = {};
    $scope.listInterviewScheduleAll = [];
    $scope.scheduleRange = {
      search: ""
    };


    // default
    var date = new Date();
    var vFirstDate = new Date(date.getFullYear(), date.getMonth(), 1);
    var vLastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);
    $scope.scheduleRange.startDate = $filter("date")(vFirstDate, "yyyy-MM-dd");
    $scope.scheduleRange.endDate = $filter("date")(vLastDate, "yyyy-MM-dd");
    $scope.scheduleRange.interviewStatus = '';
    $scope.scheduleRange.name = '';
    $scope.scheduleRange.jobRequisition = '';


    $scope.gotoDetail = function (applicantId) {
      $state.go("app.applicant-selection", {
        applicantId: applicantId
      });
    };

    var successInterviewScheduleAll = function (res) {
      if (res != null) {
        $scope.listInterviewScheduleAll = [];
        $scope.listInterviewScheduleAll = res.content;
        var totalR = res.totalRecord; // 42
        var countPages = totalR / size;
        var latestPage = Math.ceil(countPages);

        // calculate last 
        var fixLatestPage = latestPage - 1;

        if (res.page == fixLatestPage) {
          $scope.latest = 1;
        }

        if (res.content != null) {
          if ($scope.listInterviewScheduleAll.length < size)
            $scope.isLoadMoreShow = false;
          else
            $scope.isLoadMoreShow = true;
        } else {
          $scope.isLoadMoreShow = false;
        }

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    function getInterviewScheduleAll(scheduleRange) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var startDate = $filter('date')(scheduleRange.startDate, "yyyy-MM-dd");;
      var endDate = $filter('date')(scheduleRange.endDate, "yyyy-MM-dd");

      // Pass, Failed, Divert, Skip
      var status = scheduleRange.interviewStatus;
      var name = scheduleRange.name;
      var jobRequisition = scheduleRange.jobRequisition;
      var page = 0;
      var size = 10;

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicantSelection?startDate=' + startDate +
        '&endDate=' + endDate + '&name=' + name + '&jobReq=' + jobRequisition;

      if (status !== '' || status !== undefined) {
        urlApi = urlApi + '&status=' + status
      }

      urlApi = urlApi + '&page=' + page + '&size=' + size;

      Main.requestApi(accessToken, urlApi, successInterviewScheduleAll, $scope.errorRequest);
    }

    $ionicModal.fromTemplateUrl('modalAllScheduleAdmin.html', {
      id: '0',
      scope: $scope
    }).then(function (modal) {
      $scope.modalAllSchedule = modal;
    });

    $scope.openModalAllSchedule = function () {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      $scope.modalAllSchedule.show();
    };

    $scope.closeModalAllSchedule = function () {
      $scope.isLoadMoreShow = false;
      $scope.noDataShow = false;
      $scope.modalAllSchedule.hide();

      // $scope.scheduleRange.startDate = '';
      // $scope.scheduleRange.endDate = '';
      $scope.scheduleRange.interviewStatus = '';
    };

    var startDatePicker = {
      callback: function (val) { //Mandatory
        $scope.scheduleRange.startDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openStartDatePicker = function () {
      ionicDatePicker.openDatePicker(startDatePicker);
    };

    var endDatePicker = {
      callback: function (val) { //Mandatory
        $scope.scheduleRange.endDate = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: "yyyy-MM-dd",
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    $scope.openEndDatePicker = function () {
      ionicDatePicker.openDatePicker(endDatePicker);
    };

    $scope.saveData = function () {

    };


    $scope.searchScheduleRange = function () {
      getInterviewScheduleAll($scope.scheduleRange);
      $scope.closeModalAllSchedule();
    };


    function initModule() {
      getInterviewScheduleAll($scope.scheduleRange);

    }

    $scope.refresh = function(){
      $scope.scheduleRange.startDate = $filter("date")(vFirstDate, "yyyy-MM-dd");
      $scope.scheduleRange.endDate = $filter("date")(vLastDate, "yyyy-MM-dd");
      $scope.scheduleRange.interviewStatus = '';
      $scope.scheduleRange.name = '';
      $scope.scheduleRange.jobRequisition = '';
      initModule();
    };

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });

  })


  .controller('IntvSupportingDocCtrl', function (
    $stateParams,
    $ionicLoading,
    $scope,
    $state,
    Main,
    ionicSuperPopup
    ) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.listSupportingDocument = [];
    var profileData = Main.getSession("profile");
    $scope.isCareerAdmin = profileData.isCareerAdmin;

    $scope.goToUpload = function () {
      $state.go('app.interview-supporting-doc-upload', {
        'id': $stateParams.id
      });
    };

    var successListSupportingDocument = function (res) {
      if (res != null) {
        $scope.listSupportingDocument = [];
        $scope.listSupportingDocument = res;

        $ionicLoading.hide();
        $scope.$broadcast('scroll.refreshComplete');
        $scope.$broadcast('scroll.infiniteScrollComplete');
      }
    };

    $scope.$on('$ionicView.beforeEnter', function () {
      initModule();
    });

    $scope.doDeleteDocs = function (id, name) {
      ionicSuperPopup.show({
          title: "Are you sure?",
          text: "Are you sure want to delete supporting document " + name + " ?",
          type: "warning",
          showCancelButton: true,
          confirmButtonColor: "#DD6B55",
          confirmButtonText: "Yes",
          closeOnConfirm: true
        },
        function (isConfirm) {
          if (isConfirm) {
            doPostDeleteDocument(id);
          }
        });
    };

    $scope.doDownloadDocs = function (docPath) {
      window.open(encodeURI(docPath), "_system", "location=yes");
      return false;
    };

    function getListSupportingDocument() {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/applicantSelection/' + $stateParams.id + '/document';

      Main.requestApi(accessToken, urlApi, successListSupportingDocument, $scope.errorRequest);
    }

    function initModule() {
      getListSupportingDocument();
    }

    function doPostDeleteDocument(documentId) {
      if ($scope.image != "") {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });
        var objData = {};
        objData.documentId = documentId;

        var accessToken = Main.getSession("token").access_token;
        var urlApi = Main.getUrlApi() + '/api/applicantSelection/' + $stateParams.id +
          '/document/delete';
        var data = JSON.stringify(objData);

        Main.postRequestApi(accessToken, urlApi, data, successDeleteDocument, $scope.errorRequest);
      }
    }

    var successDeleteDocument = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListSupportingDocument();
    };

  })

  .controller('IntvSupportingDocUploadCtrl', function (
    $stateParams,
    $ionicLoading,
    $scope,
    $state,
    Upload,
    Main) {
    if (Main.getSession("token") == null || Main.getSession("token") == undefined) {
      $state.go("login");
    }

    $scope.supportingDocument = {};
    $scope.image = [];
    $scope.data = {};

    $scope.appMode = Main.getAppMode();

    // use this function with ng-file-upload
    $scope.uploadDocument = function (dataFile) {
      if (validateSupportingDocument($scope.supportingDocument, dataFile)) {
        doPostDocument(dataFile);
      } else {
        $scope.warningAlert(messageValidation);

        // if error happen, clear the array of image
        $scope.image = [];
        // also clear the input file value.
        $('#inputImage').val('');
      }
    };

    // use this function with ng-file-upload
    function validateSupportingDocument(suppDoc, attachment) {
      var allowedFileType = ["application/pdf", "image/png", "image/jpeg", "image/gif"];
      if (suppDoc.name == undefined || suppDoc.name == '') {
        messageValidation = "Document Name can't be empty";
        return false;
      } else if (attachment == undefined || attachment == '') {
        messageValidation = "You have to choose a Document";
        return false;
      } else if (allowedFileType.indexOf(attachment.type) < 0) {
        messageValidation = "You only can upload pdf or image";
        return false;
      } else if (attachment.size > 5242880) {
        messageValidation = "File size exceeded";
        return false;
      }
      return true;
    }

    // use this function with ng-file-upload
    function doPostDocument(dataFile) {
      if (dataFile != "") {
        $ionicLoading.show({
          template: '<ion-spinner></ion-spinner>'
        });

        Upload.base64DataUrl(dataFile).then(function(url){
          var arrDocs = url.split(",");
          $scope.data.attachments = arrDocs[1];
          $scope.data.name = $scope.supportingDocument.name;

          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/applicantSelection/' + $stateParams.id +
            '/document';
          var data = JSON.stringify($scope.data);

          Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
        });
      }
    }

    var successRequest = function (res) {
      // clear the form
      $scope.supportingDocument.name = '';
      $scope.image = [];
      $('#inputImage').val('');
      
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      $state.go('app.interview-supporting-doc', {
        'id': $stateParams.id
      });
    };

    // use this function with ng-image-compress 
    // function doPostDocument() {
    //   if ($scope.image != "") {
    //     $ionicLoading.show({
    //       template: '<ion-spinner></ion-spinner>'
    //     });

    //     var arrDocs = $scope.image[0].dataURL.split(",");
    //     $scope.data.name = $scope.supportingDocument.name;
    //     // $scope.data.attachments = $scope.image[0].dataURL.toString();
    //     $scope.data.attachments = arrDocs[1];

    //     var accessToken = Main.getSession("token").access_token;
    //     var urlApi = Main.getUrlApi() + '/api/applicantSelection/' + $stateParams.id +
    //       '/document';
    //     var data = JSON.stringify($scope.data);

    //     Main.postRequestApi(accessToken, urlApi, data, successRequest, $scope.errorRequest);
    //   }
    // }

    // use this function with ng-image-compress 
    // function validateSupportingDocument(suppDoc, attachment) {
    //   var allowedFileType = ["application/pdf", "image/png", "image/jpeg", "image/gif"];

    //   if (suppDoc.name == undefined || suppDoc.name == '') {
    //     messageValidation = "Document Name can't be empty";
    //     return false;
    //   } else if (attachment.length <= 0) {
    //     messageValidation = "You have to choose a Document";
    //     return false;
    //   } else if (allowedFileType.indexOf(attachment[0].file.type) < 0) {
    //     messageValidation = "You only can upload pdf or image";
    //     return false;
    //   } else if (attachment[0].file.size > 5242880) {
    //     messageValidation = "File size exceeded";
    //     return false;
    //   }
    //   return true;
    // }


    // use this function with ng-image-compress 
    // $scope.uploadDocument = function () {
    //   if (validateSupportingDocument($scope.supportingDocument, $scope.image)) {
    //     doPostDocument();
    //   } else {
    //     $scope.warningAlert(messageValidation);

    //     // if error happen, clear the array of image
    //     $scope.image = [];
    //     // also clear the input file value.
    //     $('#inputImage').val('');
    //   }
    // }
  });
