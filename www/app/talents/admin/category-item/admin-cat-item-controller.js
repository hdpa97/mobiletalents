angular
  .module("admin.categoryitem.controllers", [])
  .controller("AdminCategoryItemCtrl", function (
    $scope,
    $state,
    $ionicLoading,
    $ionicModal,
    $ionicPopup,
    globalConstant,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.listCategory = [];
    $scope.globalConstant = globalConstant;
    $scope.formCategory = {
      id: '',
      name: '',
    };

    $scope.filter = {
      searchText: '',
    };

    $ionicModal.fromTemplateUrl('modalFilter.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalFilter = modal;
    });

    $scope.gotoDetailCategory = function (id) {
      $state.go('app.admincatitemdetail', {
        'id': id
      });
    };

    $scope.confirmDeleteCategory = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this Category ?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/admin/pattern/delete';
          Main.postRequestApi(accessToken, urlApi, data, successDeleteCategory, $scope.errorRequest);
        }
      });
    };

    $scope.openModalFilter = function () {
      $scope.modalFilter.show();
    };

    $scope.closeModalFilter = function () {
      $scope.modalFilter.hide();
    };

    var successDeleteCategory = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListCategory();
    };

    var successRequestListCategory = function (res) {
      if (res != null) {
        $scope.listCategory = res;
      }
      $ionicLoading.hide();
    };

    function getListCategory() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/pattern";
      Main.requestApi(accessToken, urlApi, successRequestListCategory, $scope.errorRequest);
    }

    function initModule() {
      getListCategory();
    }

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });
  })

  .controller("AdminCategoryItemDetailCtrl", function (
    $scope,
    $state,
    $ionicLoading,
    $ionicModal,
    $stateParams,
    $ionicPopup,
    $ionicScrollDelegate,
    $ionicHistory,
    Main,
    ionicDatePicker,
    globalConstant
  ) {

    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    var size = Main.getDataDisplaySize();
    var indexPaging = 0;

    $ionicModal.fromTemplateUrl('modalItem.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalItem = modal;
    });

    $ionicModal.fromTemplateUrl('components/modalLov/modalShift.html', {
      id: '0',
      scope: $scope,
      focusFirstInput: true,
    }).then(function (modal) {
      $scope.modalShift = modal;
    });

    $scope.addItem = function () {
      $scope.formItem = {
        patternNo: '',
        shiftId: '',
      };
      $scope.modalItem.show();
    };

    $scope.openTPStartTimeDetail = function () {
      ionicTimePicker.openTimePicker(tpStartTimeDetail);
    };

    $scope.openDPDate = function () {
      ionicDatePicker.openDatePicker(datePickerDate);
    };

    var successSubmitFormCategory = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);

      // create new
      if ($scope.patternId == 0) {
        if (res.id != null && res.id != undefined) {
          $scope.goBack('app.adminpattern');
        }
      }
    };

    var successSubmitFormItem = function (res) {
      $scope.closeModal();
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListItem();
    };

    var successRequestListItem = function (res) {
      if (res != null) {
        $scope.listItem = res;
      }
      $ionicLoading.hide();
    };

    var successDeleteItem = function (res) {
      $ionicLoading.hide();
      $scope.successAlert(res.message);
      getListItem();
    };

    var successRequestItemById = function (res) {
      if (res != null && res != '') {
        $scope.formItem = res;
        $scope.formItem.shiftId = res.shift.id;
        $scope.formItem.shiftName = res.shift.name;
      }
      $ionicLoading.hide();
    };

    var successRequestItem = function (res) {
      if (res != null && res != '') {
        $scope.frmPattern = res;
      }
      $ionicLoading.hide();
    };

    var datePickerDate = {
      callback: function (val) { //Mandatory
        $scope.formItem.date = val;
      },
      inputDate: new Date(), //Optional
      mondayFirst: true, //Optional
      dateFormat: globalConstant.dateFormat,
      closeOnSelect: false, //Optional
      templateType: 'popup' //Optional
    };

    function getItem() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/pattern/" + $scope.patternId;
      Main.requestApi(accessToken, urlApi, successRequestItem, $scope.errorRequest);
    }

    function getListItem() {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/pattern/" + $scope.patternId + "/detail";
      Main.requestApi(accessToken, urlApi, successRequestListItem, $scope.errorRequest);
    }

    function getItemById(id) {
      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + '/api/admin/pattern/' + $scope.patternId + '/detail/' + id;
      Main.requestApi(accessToken, urlApi, successRequestItemById, $scope.errorRequest, $scope.closeModal);
    }

    function verificationForm(form) {
      return true;
    }

    function initModule() {
      $scope.patternId = $stateParams.id;

      $scope.listItem = [];
      $scope.globalConstant = globalConstant;
      $scope.findByShift = {
        search: ""
      };
      $scope.module = {};
      $scope.frmCategory = {
        name: '',
        description: '',
        activeFlag: true,
      };
      $scope.chooseTab('general');
    }

    $scope.editItem = function (id) {
      getItemById(id);
      $scope.modalItem.show();
    };

    $scope.closeModal = function () {
      $scope.modalItem.hide();
    };

    $scope.confirmDeleteItem = function (id) {
      $ionicPopup.confirm({
        title: 'Confirm',
        template: '<h5>Are you sure to delete this Item?</h5>',
        cancelText: 'Cancel',
        okText: 'Yes'
      }).then(function (res) {
        if (res) {
          $ionicLoading.show({
            template: '<ion-spinner></ion-spinner>'
          });
          var data = {
            id: id,
          };
          data = JSON.stringify(data);
          var accessToken = Main.getSession("token").access_token;
          var urlApi = Main.getUrlApi() + '/api/admin/patternDetail/delete';
          Main.postRequestApi(accessToken, urlApi, data, successDeletePatternDetail, $scope.errorRequest, $scope.closeModal);
        }
      });
    };

    $scope.submitFormItem = function () {
      if (verificationForm($scope.formItem)) {

        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>"
        });
        var accessToken = Main.getSession("token").access_token;
        var urlApi =
          Main.getUrlApi() + "/api/admin/patternDetail";

        var data = $scope.formItem;
        data.patternId = $scope.patternId;
        data.id = $scope.formItem.id == null ? null : $scope.formPatternDetail.id;

        data = JSON.stringify(data);

        Main.postRequestApi(
          accessToken,
          urlApi,
          data,
          successSubmitFormItem,
          $scope.errorRequest,
          $scope.closeModal()
        );
      } else {
        $scope.warningAlert(messageValidation);
      }
    };

    $scope.doResetPaging = function () {
      indexPaging = 0;
    };

    $scope.chooseTab = function (tab) {
      $scope.module.type = tab;

      if (tab == 'general') {
        $scope.data = {};
        if ($scope.approvalGroupId != 0) {
          getItem();
        }
      } else if (tab == 'detail') {
        $scope.listItem = [];
        getListItem();
      }
    };

    $scope.submitFormCategory = function () {
      $ionicLoading.show({
        template: "<ion-spinner></ion-spinner>"
      });

      var accessToken = Main.getSession("token").access_token;
      var urlApi =
        Main.getUrlApi() + "/api/admin/pattern/";

      var data = $scope.frmCategory;
      data.id = $scope.frmCategory.id == '' ? '' : $scope.frmPattern.id;
      data = JSON.stringify(data);

      Main.postRequestApi(
        accessToken,
        urlApi,
        data,
        successSubmitFormCategory,
        $scope.errorRequest
      );
    };

    $scope.$on("$ionicView.beforeEnter", function () {
      initModule();
    });

  });
