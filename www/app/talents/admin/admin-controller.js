angular
  .module("admin.controllers", [])
  .controller("AdministrationCtrl", function (
    $scope,
    $state,
    $ionicModal,
    $ionicLoading,
    $ionicScrollDelegate,
    Main
  ) {
    if (
      Main.getSession("token") == null ||
      Main.getSession("token") == undefined
    ) {
      $state.go("login");
    }

    $scope.profile = Main.getSession("profile");

    var size = Main.getDataDisplaySize();
    var indexPaging = 0;

    // $ionicModal.fromTemplateUrl('components/modalLov/modalEmployee.html', {
    //   id: '0',
    //   scope: $scope,
    //   focusFirstInput: true,
    // }).then(function (modal) {
    //   $scope.modalEmployee = modal;
    // });

    var successRequestEmployee = function (res) {
      $scope.findStatus = true;
      $scope.noDataShow = true;
      $scope.listEmployee = [];

      $scope.listEmployee = res.content;
      $scope.isLast = res.last;
      $scope.isFirst = res.first;

      $ionicLoading.hide();
      $scope.$broadcast('scroll.refreshComplete');
      $scope.$broadcast('scroll.infiniteScrollComplete');
      $ionicScrollDelegate.scrollTop();
    };

    $scope.openModalEmployee = function () {
      $scope.doResetPaging();
      $scope.searchByEmployee(0);
      $scope.modalEmployee.show();
    };

    $scope.closeModalEmployee = function () {
      $scope.modalEmployee.hide();
      $scope.findByEmployee.search = '';
    };

    $scope.resetEmployee = function () {
      $scope.frmAdminTrx.employee = '';
      // $scope.frmAdminTrx.employeeId = '';
      $scope.frmAdminTrx.employeeNo = '';
      $scope.frmAdminTrx.employmentId = '';
    };

    $scope.loadMoreEmployee = function () {
      indexPaging++;
      $scope.listEmployee = [];
      $scope.searchByEmployee(indexPaging);

      // reset prev
      $scope.isfirst = 0;
    };

    $scope.loadPrevEmployee = function () {
      indexPaging--;
      $scope.listEmployee = [];
      $scope.searchByEmployee(indexPaging);
    };

    $scope.getValueEmployee = function (empl) {
      $scope.frmAdminTrx.employee = empl.name;
      // $scope.frmAdminTrx.employeeId = id;
      $scope.frmAdminTrx.employeeNo = empl.employeeNo;
      $scope.frmAdminTrx.employmentId = empl.employmentId;
      $scope.closeModalEmployee();

      // reset search position
      $scope.findByEmployee.search = '';

      //reset
      $scope.listEmployee = [];
    };

    $scope.searchByEmployee = function (page) {
      $scope.findStatus = false;

      $ionicLoading.show({
        template: '<ion-spinner></ion-spinner>'
      });
      var accessToken = Main.getSession("token").access_token;
      var urlApi = Main.getUrlApi() + "/api/admin/employee/find?page=" + page + "&size=" +
        size + "&q=" + encodeURIComponent($scope.findByEmployee.search);
      Main.requestApi(accessToken, urlApi, successRequestEmployee, $scope.errorRequest);
    };

    $scope.doResetPaging = function () {
      indexPaging = 0;
    };

    $scope.goToSecurity = function (module) {
      if ($scope.profile.talentsAuthorities['Security Admin']) {
        $state.go('app.adminsecurity', {
          'module': module
        });
      }
    };

    $scope.goToForm = function () {
      var targetForm = "";
      var param = {};
      switch ($scope.frmAdminTrx.action) {
        case "Sick":
          targetForm = "app.adminaddabsence";
          param = {
            category: "sick",
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "Permission":
          targetForm = "app.adminaddabsence";
          param = {
            category: "permission",
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "Leave":
          targetForm = "app.adminaddabsence";
          param = {
            category: "leave",
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "Overtime":
          targetForm = "app.adminaddovertime";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "Long Leave Cash":
          targetForm = "app.adminaddllc";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "MPP":
          targetForm = "app.adminaddmpp";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "FKP":
          targetForm = "app.adminaddfkp";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
          };
          break;
        case "Goal":
          targetForm = "app.admingoalplan";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
            employmentId: $scope.frmAdminTrx.employmentId
          };
          break;
        case "Performance":
          targetForm = "app.adminperformanceeval";
          param = {
            employeeNo: $scope.frmAdminTrx.employeeNo,
            employmentId: $scope.frmAdminTrx.employmentId
          };
      }

      $state.go(targetForm, param);

    };

    $scope.downloadUserGuide = function () {
      window.open(encodeURI(Main.getGuideAdministrationUrl()), "_system", "location=yes");
      return false;
    };

    function initMethod() {
      $scope.profile = Main.getSession("profile");
      $scope.frmAdminTrx = {
        employee: ''
      };
      $scope.findByEmployee = {
        search: ""
      };
      $scope.listEmployee = [];
    }

    $scope.$on('$ionicView.beforeEnter', function () {
      initMethod();
    });

  })

;
